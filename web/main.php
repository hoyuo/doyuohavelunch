<?php
session_start ();
include_once ('./config.php');
$is_logged = $_SESSION ['is_logged'];
if ($is_logged == 'YES') {
	$user_id = $_SESSION ['user_id'];
	$message = $user_id;
}
$mysqli = new mysqli ( DATABASE_SERVER, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME );
if (mysqli_connect_error ()) {
	exit ( 'Connect Error (' . mysqli_connect_errno () . ') ' . mysqli_connect_error () );
}

$q = "SELECT * FROM lunch WHERE idx='$user_id'";
$result = $mysqli->query ( $q );
$row = $result->fetch_array ( MYSQLI_ASSOC );
$monday = $row ['timeTable_1'];
$tueday = $row ['timeTable_2'];
$wedday = $row ['timeTable_3'];
$thuday = $row ['timeTable_4'];
$friday = $row ['timeTable_5'];
?>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="index.css">
<script src="http://code.jquery.com/jquery-latest.js"></script>
		<?php
		echo "<script type=\"text/javascript\">\n";
		echo "$(document).ready(function() {\n";
		
		for($i = 0; $i < 8; $i ++) {
			$index = $i + 1;
			if ($monday [$i] == 1) {
				echo "document.getElementById(\"mon$index\").checked = true;\n";
			}
			if ($tueday [$i] == 1) {
				echo "document.getElementById(\"tue$index\").checked = true;\n";
			}
			if ($wedday [$i] == 1) {
				echo "document.getElementById(\"wed$index\").checked = true;\n";
			}
			if ($thuday [$i] == 1) {
				echo "document.getElementById(\"thu$index\").checked = true;\n";
			}
			if ($friday [$i] == 1) {
				echo "document.getElementById(\"fri$index\").checked = true;\n";
			}
		}
		
		echo "});";
		
		echo "</script>\n";
		?>
<title>친구야 밥먹자</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="./index.html">친구야 밥먹자</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			</ul>
		</div>
	</nav>


	<div class="container">
		<form class="form-horizontal" role="form" action="./correct.php"
			method="post">
			<div class="form-group">
				<label for="ID" class="col-xs-2 control-label">시간표</label>
				<div class="col-xs-10">
					<table class="table table-bordered" style="text-align: center">
						<tr>
							<td>시간</td>
							<td>월</td>
							<td>화</td>
							<td>수</td>
							<td>목</td>
							<td>금</td>
						</tr>
						<tr>
							<td>09:00</td>
							<td><input type="checkbox" name="mon1" id="mon1"></td>
							<td><input type="checkbox" name="tue1" id="tue1"></td>
							<td><input type="checkbox" name="wed1" id="wed1"></td>
							<td><input type="checkbox" name="thu1" id="thu1"></td>
							<td><input type="checkbox" name="fri1" id="fri1"></td>
						</tr>
						<tr>
							<td>10:00</td>
							<td><input type="checkbox" name="mon2" id="mon2"></td>
							<td><input type="checkbox" name="tue2" id="tue2"></td>
							<td><input type="checkbox" name="wed2" id="wed2"></td>
							<td><input type="checkbox" name="thu2" id="thu2"></td>
							<td><input type="checkbox" name="fri2" id="fri2"></td>
						</tr>
						<tr>
							<td>11:00</td>
							<td><input type="checkbox" name="mon3" id="mon3"></td>
							<td><input type="checkbox" name="tue3" id="tue3"></td>
							<td><input type="checkbox" name="wed3" id="wed3"></td>
							<td><input type="checkbox" name="thu3" id="thu3"></td>
							<td><input type="checkbox" name="fri3" id="fri3"></td>
						</tr>
						<tr>
							<td>12:00</td>
							<td><input type="checkbox" name="mon4" id="mon4"></td>
							<td><input type="checkbox" name="tue4" id="tue4"></td>
							<td><input type="checkbox" name="wed4" id="wed4"></td>
							<td><input type="checkbox" name="thu4" id="thu4"></td>
							<td><input type="checkbox" name="fri4" id="fri4"></td>
						</tr>
						<tr>
							<td>13:00</td>
							<td><input type="checkbox" name="mon5" id="mon5"></td>
							<td><input type="checkbox" name="tue5" id="tue5"></td>
							<td><input type="checkbox" name="wed5" id="wed5"></td>
							<td><input type="checkbox" name="thu5" id="thu5"></td>
							<td><input type="checkbox" name="fri5" id="fri5"></td>
						</tr>
						<tr>
							<td>14:00</td>
							<td><input type="checkbox" name="mon6" id="mon6"></td>
							<td><input type="checkbox" name="tue6" id="tue6"></td>
							<td><input type="checkbox" name="wed6" id="wed6"></td>
							<td><input type="checkbox" name="thu6" id="thu6"></td>
							<td><input type="checkbox" name="fri6" id="fri6"></td>
						</tr>
						<tr>
							<td>15:00</td>
							<td><input type="checkbox" name="mon7" id="mon7"></td>
							<td><input type="checkbox" name="tue7" id="tue7"></td>
							<td><input type="checkbox" name="wed7" id="wed7"></td>
							<td><input type="checkbox" name="thu7" id="thu7"></td>
							<td><input type="checkbox" name="fri7" id="fri7"></td>
						</tr>
						<tr>
							<td>16:00</td>
							<td><input type="checkbox" name="mon8" id="mon8"></td>
							<td><input type="checkbox" name="tue8" id="tue8"></td>
							<td><input type="checkbox" name="wed8" id="wed8"></td>
							<td><input type="checkbox" name="thu8" id="thu8"></td>
							<td><input type="checkbox" name="fri8" id="fri8"></td>
						</tr>
					</table>
				</div>
			</div>
			<!--
			<div class="form-group">
				<div class="col-xs-offset-2 col-xs-10">
					<button type="submit" class="btn btn-default">시간표 수정</button>
				</div>
			</div>
		-->
		</form>
	</div>
</body>
</html>