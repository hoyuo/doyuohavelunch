<?php
session_start ();
include_once ('./config.php');

$user_id = $_POST ['ID'];
$user_pwd = $_POST ['password'];

echo "<meta charset=\"utf-8\">";
$mysqli = new mysqli ( DATABASE_SERVER, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_NAME );
if (mysqli_connect_error ()) {
	exit ( 'Connect Error (' . mysqli_connect_errno () . ') ' . mysqli_connect_error () );
}

$q = "SELECT * FROM lunch WHERE id='$user_id'";
$result = $mysqli->query ( $q );

if ($result->num_rows == 1) {
	// 해당 ID 의 회원이 존재할 경우
	// 암호가 맞는지를 확인
	$encryped_pass = sha1 ( $user_pwd );
	$row = $result->fetch_array ( MYSQLI_ASSOC );
	if ($row ['pwd'] == $encryped_pass) {
		// 올바른 정보
		$_SESSION ['is_logged'] = 'YES';
		$_SESSION ['user_id'] = $row ['idx'];
		header ( "Location: ./main.php" );
		exit ();
	} else {
		// 암호가 틀렸음
		$_SESSION ['is_logged'] = 'NO';
		$_SESSION ['user_id'] = '';
		echo "<script>alert(\"비밀번호가 틀렸습니다.\");history.back();</script>";
	}
} else {
	// 없거나, 비정상
	echo "<script>alert(\"없는 Email입니다\");history.back();</script>";
}

?>
