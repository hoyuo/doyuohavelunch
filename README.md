DoyouHaveLunch
==============
안드로이드프로젝트
친구야 밥먹자라는 주제를 가지고 어플을 작성합니다.
+ 개발과정은 [블로그](http://hoyuo.tistory.com/category/Programming/Android) 에 포스팅 예정입니다.
+ 서버는 우분트 Ubuntu 14.04.1 LTS
+ 웹 개발은 HTML5 + CSS3 + PHP 로 진행합니다
+ 안드로이드 개발은 GPS와 XML파싱을 이용해서 간단한 어플리케이션 입니다.

빌드방법
==============
- google-play-services_lib라이브러리를 먼저 이클립스 프로젝트에 추가 시킵니다.
- 프로젝트 속성 - Android에서 밑에 보이는 Library에서 Is Library를 채크합니다.
- DoyouHaveLunch 라이브러리를 추가 합니다. 
- 프로젝트 속성 - Android에서 밑에 보이는 Library에 Add 버튼을 눌러서 google-play-services_lib를 추가합니다.
- 프로젝트 속성 - Java Build Path에서 jericho-android.3.1.jar이 있는지 확인합니다. 없으면 Add External JARs 버튼을 클릭해서 libs폴더에서 jericho-android.3.1.jar 추가합니다. 
- 구글API콘솔 키값을 사용하기 때문에 API콘솔값을 인증받은뒤 androidManifest.xml파일에 com.google.android.maps.v2.API_KEY 항목 value 값에 넣어줍니다.
- 빌드가 완료 됩니다.

- 첨부로 해둔 DoyouHaveLunch.apk를 가지고 설치를 하셔서 보셔도 됩니다.

사용방법
==============
- [가입페이지](http://hoyuo.me/rice)에서 가입을 하신뒤 사용을 하시면 됩니다.
- 스플래쉬 화면은 약 1.5초가 지나면 로그인 화면으로 바뀝니다.
- 로그인 화면에서는 가입페이지에서 가입한 아이디와 비밀번호를 입력하시면 자동로그인 방식으로 로그인이 됩니다.
- 로그인이 완료되고 처음보이는 페이지는 현재 공강인 친구들의 목록과 추가적으로 나의 다음 공강시간을 보여줍니다. 전화버튼을 클릭하면 바로 친구한테 연락이 갑니다.
- 두번째 화면은 오늘의 학관 식단 메뉴표 입니다.
- 세번째 화면은 현재 친구들의 위치가 표시 됩니다. 구글맵의 문제인지 저의 처리능력 부족인지 모르겠지만 약간의 로딩 시간이 걸립니다.
- 네번째 화면은 도움창입니다. 사용하다가 불편하신점에 대해서 이메일을 보내주시면 됩니다. 로그아웃버튼은 자동로그인 방식이기때문에 로그인을 해제하기 위해서 만들었습니다.
