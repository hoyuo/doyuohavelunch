package me.hoyuo.doyouhavelunch;

import java.util.ArrayList;

import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class FriendAdapter extends BaseAdapter {
	private Context mContext;

	private TextView friendName;
	private TextView friendPhoneNumber;

	ArrayList<Friend> mFriend;

	public FriendAdapter(Context context, ArrayList<Friend> friend) {
		mContext = context;
		mFriend = friend;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFriend.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mFriend.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final int pos = position;
		View v = convertView;

		if (v == null) {
			v = ((LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.friend, null);
		}

		friendName = (TextView) v.findViewById(R.id.friendName);
		friendPhoneNumber = (TextView) v.findViewById(R.id.friendPhoneNumber);

		friendName.setText(mFriend.get(position).friendName);
		friendPhoneNumber.setText(mFriend.get(position).friendPhoneNumber);
		ImageButton btn = (ImageButton) v.findViewById(R.id.callButton);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String str = mFriend.get(pos).friendPhoneNumber;
				Uri uri = Uri.parse("tel:" + str);
				Intent i = new Intent(Intent.ACTION_DIAL, uri);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mContext.startActivity(i);
			}
		});
		return v;
	}

}
