package me.hoyuo.doyouhavelunch;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.StrictMode;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class GPSTask {

	public ArrayList<GPS> XmlPassing() {
		// TODO Auto-generated method stub
		ArrayList<GPS> gps = new ArrayList<GPS>();

		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			GPS temp = null;
			URL url = new URL("http://hoyuo.me/rice/gps.php");
			XmlPullParserFactory parserFactory = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserFactory.newPullParser();
			parser.setInput(url.openStream(), "utf-8");

			int parserEvent = parser.getEventType();
			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {
				case XmlPullParser.START_DOCUMENT:
					break;

				case XmlPullParser.START_TAG:
					String name = parser.getName();
					if (name != null && name.equals("node")) {
						String userID = parser
								.getAttributeValue(null, "userID");
						String latitude = parser.getAttributeValue(null,
								"Latitude");
						String longitude = parser.getAttributeValue(null,
								"Longitude");
						String userName = getName(userID);
						temp = new GPS(userID, userName,
								Double.valueOf(latitude),
								Double.valueOf(longitude));
					}
					break;

				case XmlPullParser.TEXT:
					break;

				case XmlPullParser.END_TAG:
					if (temp != null) {
						gps.add(temp);
					}
					temp = null;
				}
				parserEvent = parser.next();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return gps;
	}

	public void DataInput(String dbId, LatLng curPoint) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String _id = "id=" + dbId;
		String _latitude = "latitude=" + curPoint.latitude;
		String _longitude = "longitude=" + curPoint.longitude;

		String loginURL = "http://hoyuo.me/rice/gpsInput.php?" + _id + "&"
				+ _latitude + "&" + _longitude;

		String ret = null;
		try {
			URL nURL = new URL(loginURL);
			InputStream html = nURL.openStream();
			Source source = new Source(new InputStreamReader(html, "utf-8"));

			source.fullSequentialParse();

			Element title = source.getAllElements(HTMLElementName.BODY).get(0);
			ret = title.getTextExtractor().toString();
			Log.i("Check", ret);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getName(String id) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String _id = "id=" + id;
		String loginURL = "http://hoyuo.me/rice/name.php?" + _id;

		String ret = null;
		try {
			URL nURL = new URL(loginURL);
			InputStream html = nURL.openStream();
			Source source = new Source(new InputStreamReader(html, "utf-8"));

			source.fullSequentialParse();

			Element title = source.getAllElements(HTMLElementName.BODY).get(0);
			ret = title.getTextExtractor().toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
