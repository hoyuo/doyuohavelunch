package me.hoyuo.doyouhavelunch;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FoodAdapter extends BaseAdapter {
	private Context mContext;

	private TextView menuTitle;
	private TextView menuName;
	private TextView menuAbout;

	private ArrayList<Food> mFoodData;

	public FoodAdapter(Context context, ArrayList<Food> foodDate) {
		super();
		mContext = context;
		mFoodData = foodDate;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mFoodData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mFoodData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View v = convertView;

		if (v == null) {
			v = ((LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.foodmenu, null);
		}

		menuTitle = (TextView) v.findViewById(R.id.menuTitle);
		menuName = (TextView) v.findViewById(R.id.menuName);
		menuAbout = (TextView) v.findViewById(R.id.menuAbout);

		menuTitle.setText(mFoodData.get(position).title);
		if (mFoodData.get(position).segment.equals("1")) {
			menuName.setText("조중식");
		} else {
			menuName.setText("석식");
		}
		menuAbout.setText(mFoodData.get(position).menu);
		return v;
	}

	public void add(Food food) {
		mFoodData.add(food);
	}
}
