package me.hoyuo.doyouhavelunch;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class SettingActivity extends Fragment {

	View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		v = inflater.inflate(R.layout.activity_setting, container, false);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		Button logoutBtn = (Button) v.findViewById(R.id.logoutBtn);
		logoutBtn.setOnClickListener(logoutListener);
		Button emailBtn = (Button) v.findViewById(R.id.emailBtn);
		emailBtn.setOnClickListener(emailListener);

		super.onActivityCreated(savedInstanceState);
	}

	Button.OnClickListener logoutListener = new OnClickListener() {
		@Override
		public void onClick(View v) { // TODO Auto-generated method
			SharedPreferences settings = v.getContext().getSharedPreferences(
					"DBid", Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("DBid", "");
			editor.commit();

			getActivity().finish();
			Intent intent = new Intent();
			intent.setClassName("me.hoyuo.doyouhavelunch",
					"me.hoyuo.doyouhavelunch.LoginActivity");
			startActivity(intent);
		}
	};

	Button.OnClickListener emailListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Uri uri = Uri.parse("mailto:psh9907@gmail.com");
			Intent i = new Intent(Intent.ACTION_SENDTO, uri);
			startActivity(i);
		}
	};
}
