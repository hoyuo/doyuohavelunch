package me.hoyuo.doyouhavelunch;

public class Friend {
	public String idx;
	public String friendName;
	public String friendPhoneNumber;
	public String friendTimeTable;

	public Friend(String idx, String friendName, String friendPhoneNumber,
			String friendTimeTable) {
		super();
		this.idx = idx;
		this.friendName = friendName;
		this.friendPhoneNumber = friendPhoneNumber;
		this.friendTimeTable = friendTimeTable;
	}

	public String getFriendName() {
		return friendName;
	}

	public String getFriendPhoneNumber() {
		return friendPhoneNumber;
	}

	public String getFriendTimeTable() {
		return friendTimeTable;
	}
}
