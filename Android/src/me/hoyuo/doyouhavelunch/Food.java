package me.hoyuo.doyouhavelunch;

public class Food {
	public String segment;
	public String title;
	public String menu;

	public Food(String segment, String title, String menu) {
		super();
		this.segment = segment;
		this.title = title;
		this.menu = menu;
	}

	public String getSegment() {
		return segment;
	}

	public String getTitle() {
		return title;
	}

	public String getMenu() {
		return menu;
	}

	public String toString() {
		return segment + title + menu;
	}

}
