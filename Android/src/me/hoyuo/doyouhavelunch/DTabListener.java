package me.hoyuo.doyouhavelunch;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class DTabListener implements ActionBar.TabListener {
	private Fragment mFragment;
	private final Activity mActivity;
	private final String mFragName;

	public DTabListener(Activity activity, String fragName) {
		mActivity = activity;
		mFragName = fragName;
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		mFragment = Fragment.instantiate(mActivity, mFragName);
		ft.add(android.R.id.content, mFragment);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		ft.remove(mFragment);
		mFragment = null;

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
