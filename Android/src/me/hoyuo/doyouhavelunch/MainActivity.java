package me.hoyuo.doyouhavelunch;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar abar = getActionBar();
		abar.setStackedBackgroundDrawable(new ColorDrawable(0xffffff));
		abar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		abar.setDisplayShowHomeEnabled(false);
		abar.setTitle("");

		Tab tab01 = abar.newTab();
		tab01.setIcon(R.drawable.menu1);
		tab01.setTabListener(new DTabListener(this, TimeTableActivity.class
				.getName()));
		abar.addTab(tab01);

		Tab tab02 = abar.newTab();
		tab02.setIcon(R.drawable.menu2);
		tab02.setTabListener(new DTabListener(this, MenuActivity.class
				.getName()));
		abar.addTab(tab02);

		Tab tab03 = abar.newTab();
		tab03.setIcon(R.drawable.menu3);
		tab03.setTabListener(new DTabListener(this, MapActivity.class.getName()));
		abar.addTab(tab03);

		Tab tab04 = abar.newTab();
		tab04.setIcon(R.drawable.menu4);
		tab04.setTabListener(new DTabListener(this, SettingActivity.class
				.getName()));
		abar.addTab(tab04);
	}
}
