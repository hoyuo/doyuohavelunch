package me.hoyuo.doyouhavelunch;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class TimeTableActivity extends Fragment {
	ArrayList<Friend> friend = new ArrayList<Friend>();
	FriendAdapter adapter;
	View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		v = inflater.inflate(R.layout.activity_time_table, container, false);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		ListView lv = (ListView) v.findViewById(R.id.friendList);
		SharedPreferences settings = v.getContext().getSharedPreferences(
				"DBid", Context.MODE_PRIVATE);
		String dbId = settings.getString("DBid", "");
		Calendar cal = Calendar.getInstance();
		int dayNum = cal.get(Calendar.DAY_OF_WEEK);
		FriendTask friendTask = new FriendTask();
		friend = friendTask.XmlPassing(dbId, dayNum);

		adapter = new FriendAdapter(getActivity().getApplicationContext(),
				friend);
		lv.setAdapter(adapter);
		adapter.notifyDataSetChanged();

		int temp = friendTask.NextHour();

		TextView tv = (TextView) v.findViewById(R.id.dateView);
		String output;

		if (temp != -1) {
			output = (temp + 9) + "시";
		} else {
			output = "수업 끝!";
		}
		tv.setText(output);
		super.onActivityCreated(savedInstanceState);
	}
}