package me.hoyuo.doyouhavelunch;

import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.StrictMode;

public class MenuTask {

	public ArrayList<Food> XmlPassing() {
		ArrayList<Food> food = null;
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			food = new ArrayList<Food>();
			Food temp = null;
			java.util.Calendar cal = java.util.Calendar.getInstance();
			int year = cal.get(cal.YEAR);
			int month = cal.get(cal.MONTH) + 1;
			int date = cal.get(cal.DATE);
			String day = year + "-";
			if (month < 10) {
				day += "0";
			}
			day += month + "-";
			if (date < 10) {
				day += "0";
			}
			day += date;
			URL url = new URL("http://hoyuo.me/rice/foodMenu.php?day=" + day);
			XmlPullParserFactory parserFactory = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserFactory.newPullParser();
			parser.setInput(url.openStream(), "utf-8");

			int parserEvent = parser.getEventType();
			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					String name = parser.getName();
					if (name != null && name.equals("node")) {
						String segment = parser.getAttributeValue(null,
								"segment");
						String title = parser.getAttributeValue(null, "title");
						String menu = parser.getAttributeValue(null, "menu");
						temp = new Food(segment, title, menu);
					}
					break;

				case XmlPullParser.TEXT:
					break;

				case XmlPullParser.END_TAG:
					if (temp != null) {
						food.add(temp);
					}
					temp = null;
				}

				parserEvent = parser.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return food;
	}

}
