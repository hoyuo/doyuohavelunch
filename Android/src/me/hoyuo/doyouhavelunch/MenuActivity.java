package me.hoyuo.doyouhavelunch;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class MenuActivity extends Fragment {
	ArrayList<Food> food = new ArrayList<Food>();
	FoodAdapter adapter;
	View view;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.activity_menu, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		int year = cal.get(cal.YEAR);
		int month = cal.get(cal.MONTH) + 1;
		int date = cal.get(cal.DATE);
		String day = year + "-";

		if (month < 10) {
			day += "0";
		}
		day += month + "-";
		if (date < 10) {
			day += "0";
		}
		day += date;

		TextView dateView = (TextView) view.findViewById(R.id.dateView);
		dateView.setText(day);

		ListView list = (ListView) view.findViewById(R.id.foodmenu);
		// 웹에서 가져오는걸 하고
		food = new MenuTask().XmlPassing();
		adapter = new FoodAdapter(getActivity().getApplicationContext(), food);
		list.setAdapter(adapter);
		adapter.notifyDataSetChanged();

		super.onActivityCreated(savedInstanceState);
	}
}
