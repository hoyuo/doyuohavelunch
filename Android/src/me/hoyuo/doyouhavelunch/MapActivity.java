package me.hoyuo.doyouhavelunch;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Fragment {
	MapView mMapView;
	Location location = null;
	private GoogleMap mGoogleMap = null;
	ArrayList<GPS> mGps = new ArrayList<GPS>();
	View v;
	String dbId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		v = inflater.inflate(R.layout.activity_map, container, false);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mMapView = (MapView) v.findViewById(R.id.map);
		mMapView.onCreate(savedInstanceState);
		mMapView.onResume();// needed to get the map to display immediately
		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
		mGoogleMap = mMapView.getMap();
		SharedPreferences settings = v.getContext().getSharedPreferences(
				"DBid", Context.MODE_PRIVATE);
		dbId = settings.getString("DBid", "");

		AddMarkers();

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		mGoogleMap.setMyLocationEnabled(true);
		mMapView.onResume();
		startLocationService(getActivity().getApplicationContext());
		onMyLocationChange();
	}

	@Override
	public void onPause() {
		super.onPause();
		mGoogleMap.setMyLocationEnabled(false);
		mMapView.onPause();
	}

	public void onMyLocationChange() {
		LatLng curPoint;
		if (location != null) {
			curPoint = new LatLng(location.getLatitude(),
					location.getLongitude());
		} else {
			curPoint = new LatLng(37.2782895, 127.9100443);
		}
		mGoogleMap.animateCamera(CameraUpdateFactory
				.newLatLngZoom(curPoint, 15));
		mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		GPSTask gpsTask = new GPSTask();
		gpsTask.DataInput(dbId, curPoint);
	}

	private void startLocationService(Context ctx) {
		LocationManager manager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);
		location = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	private void AddMarkers() {
		GPSTask gpsTask = new GPSTask();
		mGps = gpsTask.XmlPassing();

		for (int i = 0; i < mGps.size(); i++) {
			if (!mGps.get(i).userID.equals(dbId)) {
				double latitude = mGps.get(i).latitude;
				double longitude = mGps.get(i).longtitude;

				MarkerOptions marker = new MarkerOptions().position(
						new LatLng(latitude, longitude)).title(
						mGps.get(i).userName);

				marker.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.pin));

				// adding marker
				Marker temp = mGoogleMap.addMarker(marker);
			}
		}
	}
}
