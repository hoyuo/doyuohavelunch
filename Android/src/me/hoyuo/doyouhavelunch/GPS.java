package me.hoyuo.doyouhavelunch;

public class GPS {
	public String userID;
	public String userName;
	public double latitude;
	public double longtitude;

	public String getUserID() {
		return userID;
	}

	public String getUserName() {
		return userName;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongtitude() {
		return longtitude;
	}

	public GPS(String userID, String userName, double latitude,
			double longtitude) {
		super();
		this.userID = userID;
		this.userName = userName;
		this.latitude = latitude;
		this.longtitude = longtitude;
	}

}
