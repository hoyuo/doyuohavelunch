package me.hoyuo.doyouhavelunch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
	EditText id;
	EditText pwd;
	String result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		startActivity(new Intent(this, SplashActivity.class));
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				TextView tv = (TextView) findViewById(R.id.loginCaption);
				tv.setTypeface(Typeface.createFromAsset(getAssets(),
						"SourceHanSansKR-Normal.otf"));
				id = (EditText) findViewById(R.id.idInput);
				pwd = (EditText) findViewById(R.id.passwordInput);

				Button loginBtn = (Button) findViewById(R.id.loginBtn);
				loginBtn.setOnClickListener(loginListener);
				Button singupBtn = (Button) findViewById(R.id.SingupBtn);
				singupBtn.setOnClickListener(SingupListener);

				SharedPreferences settings = getSharedPreferences("DBid",
						Context.MODE_PRIVATE);
				String dbId = settings.getString("DBid", "");

				if (dbId != "") {
					Intent intent = new Intent();
					intent.setClassName("me.hoyuo.doyouhavelunch",
							"me.hoyuo.doyouhavelunch.MainActivity");
					startActivity(intent);
					finish();
				}
			}
		}, 1500);
	}

	Button.OnClickListener loginListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			String idInput = id.getText().toString();
			String pwdInput = pwd.getText().toString();

			if (idInput.equals("") || pwdInput.equals("")) {
				new AlertDialog.Builder(LoginActivity.this)
						.setMessage("E-mail, Password를 입력해주세요")
						.setNeutralButton("닫기",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dlg,
											int sumthin) {
									}
								}).show();
			} else {
				String ret;
				ret = new LoginTask().getData(idInput, pwdInput);

				if (!ret.equals("error")) {
					SharedPreferences settings = getSharedPreferences("DBid",
							Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("DBid", ret);
					editor.commit();

					Intent intent = new Intent();
					intent.setClassName("me.hoyuo.doyouhavelunch",
							"me.hoyuo.doyouhavelunch.MainActivity");
					startActivity(intent);
					finish();
				} else {
					new AlertDialog.Builder(LoginActivity.this)
							.setMessage("E-mail 또는 Password가 잘못되었습니다.")
							.setNeutralButton("닫기",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dlg, int sumthin) {
										}
									}).show();
				}
			}
		}
	};

	Button.OnClickListener SingupListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://hoyuo.me/rice/singup.html")));
		}
	};

}// LoginActivity
