package me.hoyuo.doyouhavelunch;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;
import android.os.StrictMode;

public class LoginTask {
	public String getData(String id, String pwd) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		String _id = "id=" + id;
		String _pwd = "pwd=" + pwd;
		String loginURL = "http://hoyuo.me/rice/moblie.php?" + _id + "&" + _pwd;

		String ret = null;
		try {
			URL nURL = new URL(loginURL);
			InputStream html = nURL.openStream();
			Source source = new Source(new InputStreamReader(html, "utf-8"));

			source.fullSequentialParse();

			Element title = source.getAllElements(HTMLElementName.BODY).get(0);
			ret = title.getTextExtractor().toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
