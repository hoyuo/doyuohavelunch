package me.hoyuo.doyouhavelunch;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;

public class FriendTask {
	Friend my;

	public ArrayList<Friend> XmlPassing(String dbId, int dayNum) {
		ArrayList<Friend> friends = new ArrayList<Friend>();

		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);

			Friend temp = null;
			URL url = new URL("http://hoyuo.me/rice/timetable.php?weekday="
					+ dayNum);
			XmlPullParserFactory parserFactory = XmlPullParserFactory
					.newInstance();
			XmlPullParser parser = parserFactory.newPullParser();
			parser.setInput(url.openStream(), "utf-8");

			int parserEvent = parser.getEventType();
			while (parserEvent != XmlPullParser.END_DOCUMENT) {
				switch (parserEvent) {
				case XmlPullParser.START_DOCUMENT:
					break;

				case XmlPullParser.START_TAG:
					String name = parser.getName();
					if (name != null && name.equals("node")) {
						String idx = parser.getAttributeValue(null, "IDX");
						String friendName = parser.getAttributeValue(null,
								"NAME");
						String phoneNumber = parser.getAttributeValue(null,
								"PHONENUMBER");
						String timeTable = parser.getAttributeValue(null,
								"TimeTable");
						temp = new Friend(idx, friendName, phoneNumber,
								timeTable);
					}
					break;

				case XmlPullParser.TEXT:
					break;

				case XmlPullParser.END_TAG:
					if (temp != null) {
						friends.add(temp);
					}
					temp = null;
				}
				parserEvent = parser.next();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return FindFriend(dbId, friends);
	}

	private ArrayList<Friend> FindFriend(String dbId, ArrayList<Friend> friends) {
		ArrayList<Friend> ret = new ArrayList<Friend>();
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int check = HourOfCheck(hour);
		for (int i = 0; i < friends.size(); i++) {
			if (!friends.get(i).idx.equals(dbId)) {
				String temp = friends.get(i).friendTimeTable;
				if (check != -1) {
					if (temp.charAt(check) == '0') {
						ret.add(friends.get(i));
					}
				} else {
					ret.add(friends.get(i));
				}
			} else {
				my = friends.get(i);
			}
		}
		return ret;
	}

	private int HourOfCheck(int hour) {
		// TODO Auto-generated method stub
		int ret;
		switch (hour) {
		case 9:
			ret = 0;
			break;
		case 10:
			ret = 1;
			break;
		case 11:
			ret = 2;
			break;
		case 12:
			ret = 3;
			break;
		case 13:
			ret = 4;
			break;
		case 14:
			ret = 5;
			break;
		case 15:
			ret = 6;
			break;
		case 16:
			ret = 7;
			break;
		default:
			ret = -1;
			break;
		}
		return ret;
	}

	public int NextHour() {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);

		int next = HourOfCheck(hour);
		int ret = -1;

		if (next != -1) {

			while (next < 8 && my.getFriendTimeTable().charAt(next) != '0') {
				next++;
			}

			if (next < 8) {
				ret = next;
			}
		}
		return ret;
	}
}
